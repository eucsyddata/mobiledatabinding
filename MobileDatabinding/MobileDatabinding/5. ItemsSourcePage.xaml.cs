﻿using MobileDatabinding.Model;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileDatabinding
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemsSourcePage : ContentPage
    {
        PersonCN selectedPerson;
        List<PersonCN> persons;

        public ItemsSourcePage()
        {
            InitializeComponent();

            persons = new List<PersonCN>
            {
                new PersonCN { Name = "Katja", Age = 27 },
                new PersonCN { Name = "Christian", Age = 32 },
                new PersonCN { Name = "Helle", Age = 54 }
            };

            personPicker.ItemsSource = persons;
        }

        void OnPickerSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            selectedPerson = (PersonCN)picker.SelectedItem;

            if (selectedPerson != null)
            {
                stackLayout.BindingContext = selectedPerson;
            }
        }

        private void OnShowAge(object sender, EventArgs e)
        {
            if (selectedPerson != null)
            {
                DisplayAlert("Age",
                selectedPerson.Name + " er " + selectedPerson.Age + " år!",
                "OK");
            }
        }

        private void OnMakeOlder(object sender, EventArgs e)
        {
            if (selectedPerson != null)
            {
                selectedPerson.Age++;
            }
        }
    }
}