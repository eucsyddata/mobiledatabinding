﻿// Constructoren opretter et Person-object.
// En BindingContext sættes på StackLayout og nedarves til Child-kontrollerne.
// Der bindes til de enkelte Text-properties, både med Path og uden Path.

using MobileDatabinding.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileDatabinding
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SimpleDataBinding : ContentPage
    {
        Person myPerson;
        public SimpleDataBinding()
        {
            InitializeComponent();

            myPerson = new Person { Name = "Katja", Age = 27 };
            stackLayout.BindingContext = myPerson;
            // this.BindingContext = myPerson;     // Alternativt
        }
    }
}