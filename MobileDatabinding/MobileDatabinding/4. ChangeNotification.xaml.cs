﻿// 1. Først med Mode=OneWay Binding for Age:
//      Ændre alderen i TextBox og vælg Show Age og 
//      bemærk at ændring i Target ikke sendes tilbage Source (fordi Mode=OneWay).
//      Ændre Mode=TwoWay for Age og se at nu vises ShowAge korrekt.
//      Prøv også Mode=Default eller fjerne Mode og se at TwoWay vælges automatisk!

//  2. Når Source opdateres følger Target ikke med:
//      Når knappen MakeOlder aktiveres, opdateres alderen IKKE automatisk i Entry-feltet. 
//      Modellen er opdateret, hvilket klik på ShowAge viser. Datamodel og GUI er altså ikke i Sync!
//      INotificationPropertyChanged implementeres, benyt PersonCN klassen istedet for Person.

using MobileDatabinding.Model;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileDatabinding
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChangeNotification : ContentPage
    {
        Person myPerson = new Person { Name = "Katja", Age = 27 };
        //PersonCN myPerson = new PersonCN { Name = "Katja", Age = 27 };
        public ChangeNotification()
        {
            InitializeComponent();

            stackLayout.BindingContext = myPerson;
        }

        private void OnShowAge(object sender, EventArgs e)
        {
            DisplayAlert("Age",
                myPerson.Name + " er " + myPerson.Age + " år!",
                "OK");
        }

        private void OnMakeOlder(object sender, EventArgs e)
        {
            myPerson.Age++;
        }
    }
}