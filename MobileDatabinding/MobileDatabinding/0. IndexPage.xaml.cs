﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileDatabinding
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IndexPage : ContentPage
    {
        public IndexPage()
        {
            InitializeComponent();
        }
        private void OnViewtoViewBinding(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ViewtoViewBinding(), true);
        }

        private void OnValueConverter(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ValueConverter(), true);
        }

        private void OnSimpleBinding(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SimpleDataBinding(), true);
        }

        private void OnChangeNotification(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ChangeNotification(), true);
        }

        private void OnItemsSorcePage(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ItemsSourcePage(), true);
        }

        private void OnObservableCollectionPage(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ObservableCollectionPage(), true);
        }
    }
}