﻿// 1. Først fortsættes med List<Person> collection
//      som ikke opdaterer GUI når ny person indsættes eller slettes.
// 2. Skift derefter til ObservableCollection<T>, som opdaterer
//      GUI'en automatisk, når der indsættes, slettes eller opdateres items.

using MobileDatabinding.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileDatabinding
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ObservableCollectionPage : ContentPage
    {
        PersonCN selectedPerson;

        List<PersonCN> persons;
        //ObservableCollection<PersonCN> persons;

        public ObservableCollectionPage()
        {
            InitializeComponent();

            persons = new List<PersonCN>
            //persons = new ObservableCollection<PersonCN>
            {
                new PersonCN { Name = "Katja", Age = 27 },
                new PersonCN { Name = "Christian", Age = 32 },
                new PersonCN { Name = "Helle", Age = 54 }
            };

            personPicker.ItemsSource = persons;
        }

        void OnPickerSelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            selectedPerson = picker.SelectedItem as PersonCN;

            if (selectedPerson != null)
            {
                stackLayout.BindingContext = selectedPerson;
            }
        }
         
        private void OnShowAge(object sender, EventArgs e)
        {
            if (selectedPerson != null)
            {
                DisplayAlert("Age",
                selectedPerson.Name + " er " + selectedPerson.Age + " år!",
                "OK");
            }
        }

        private void OnMakeOlder(object sender, EventArgs e)
        {
            if (selectedPerson != null)
            {
                selectedPerson.Age++;
            }
        }

        private void OnAddPerson(object sender, EventArgs e)
        {
            persons.Add(new PersonCN { Name = "New Person ", Age = 0 });
        }

        private void OnDeletePerson(object sender, EventArgs e)
        {
            if (selectedPerson != null)
            {
                persons.Remove(selectedPerson);
            }
        }
    }
}